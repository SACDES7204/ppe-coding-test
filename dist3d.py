#program to find the distance between two points in 3d space(3d-x,y,z)
import math

#Getting points from users

#Point A
x0=int(input("Enter x0 value of point A"))
y0=int(input("Enter y0 value of point A"))
z0=int(input("Enter z0 value of point A"))

#Point B
x1=int(input("Enter x1 value of point B"))
y1=int(input("Enter y1 value of point B"))
z1=int(input("Enter z1 value of point B"))


#method to find distance between two points:

def dist(x0,y0,z0,x1,y1,z1):
    distance=math.sqrt(math.pow(x1-x0,2)+math.pow(y1-y0,2)+math.pow(z1-z0,2)*1.0)
    print("distance between two points is :"distance)
    
#Calling the method

dist(x0,y0,z0,x1,y1,z1)

Print("Done !")